package model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gctech.gcm.model.GcmPushNotification;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class GcmPushNotificationTest {

    private GcmPushNotification emptyNotification;
    private GcmPushNotification fullNotification;

    @Before
    public void setup() {
        emptyNotification = new GcmPushNotificationBuilder().build();
        fullNotification = new GcmPushNotificationBuilder()
                .setTo("/topics/foo")
                .setTitle("Any title")
                .setContent("Any content")
                .setIconName("Any icon name")
                .setTime("00:00:00")
                .setTimeToLive(180)
                .build();
    }

    @Test
    public void testMarshall_whenNoFieldsProvided_thenJsonKeysCorrect() throws JsonProcessingException {
        String result = new ObjectMapper().writeValueAsString(emptyNotification);
        assertThat(result, containsString("to"));
        assertThat(result, containsString("data"));
    }

    @Test
    public void testMarshall_whenAllDataFieldsProvided_thenFullJsonCorrect() throws JsonProcessingException {
        String result = new ObjectMapper().writeValueAsString(fullNotification);
        assertThat(result, containsString("time_to_live"));
        assertThat(result, containsString("data"));
        assertThat(result, containsString("Any title"));
        assertThat(result, containsString("Any content"));
    }

    @Test
    public void testNotMarshall_whenIgnoredFieldsProvided_thenFullJsonCorrect() throws JsonProcessingException {
        String result = new ObjectMapper().writeValueAsString(fullNotification);
        assertThat(result, not(containsString("00:00:00")));
    }

    class GcmPushNotificationBuilder {

        private GcmPushNotification notification = new GcmPushNotification();

        public GcmPushNotificationBuilder setTo(String to) {
            notification.setTo(to);
            return this;
        }

        public GcmPushNotificationBuilder setTimeToLive(Number timeToLive) {
            notification.setTimeToLive(timeToLive);
            return this;
        }

        public GcmPushNotificationBuilder setTitle(String title) {
            notification.getData().setTitle(title);
            return this;
        }

        public GcmPushNotificationBuilder setContent(String content) {
            notification.getData().setContent(content);
            return this;
        }

        public GcmPushNotificationBuilder setIconName(String iconName) {
            notification.getData().setIconName(iconName);
            return this;
        }

        public GcmPushNotificationBuilder setTime(String time) {
            notification.setTime(time);
            return this;
        }

        public GcmPushNotification build() {
            return new GcmPushNotification(notification.getTo(), notification.getData().getTitle(), notification.getData().getContent(), notification.getData().getIconName());
        }

    }

}