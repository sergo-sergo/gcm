package com.gctech.gcm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

public class GcmPushNotification implements Serializable {

    private String to;
    private GcmPushNotificationData data = new GcmPushNotificationData();

    @JsonProperty("time_to_live")
    private Number timeToLive = 180;
    @JsonIgnore
    private String time;

    public GcmPushNotification() {
        setTo("");
        getData().setTitle("");
        getData().setContent("");
        getData().setIconName("");
    }

    public GcmPushNotification(String to, String title, String content, String icon) {
        setTo(to);
        getData().setTitle(title);
        getData().setContent(content);
        getData().setIconName(icon);
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Number getTimeToLive() {
        return timeToLive;
    }

    public void setTimeToLive(Number timeToLive) {
        this.timeToLive = timeToLive;
    }

    public GcmPushNotificationData getData() {
        return data;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @JsonIgnore
    public String getTitle() {
        return getData().getTitle();
    }

    @JsonIgnore
    public void setTitle(String title) {
        getData().setTitle(title);
    }

    @JsonIgnore
    public String getContent() {
        return getData().getContent();
    }

    @JsonIgnore
    public void setContent(String content) {
        getData().setContent(content);
    }

    @JsonIgnore
    public String getIconName() {
        return getData().getIconName();
    }

    @JsonIgnore
    public void setIconName(String iconName) {
        getData().setIconName(iconName);
    }

    @JsonIgnore
    public int getKeepAliveInSeconds() {
        return getData().getKeepAliveInSeconds();
    }

    @JsonIgnore
    public void setKeepAliveInSeconds(int keepAliveInSeconds) {
        getData().setKeepAliveInSeconds(keepAliveInSeconds);
    }

    public class GcmPushNotificationData {

        private String title;
        private String content;
        private String iconName;
        private int keepAliveInSeconds;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getIconName() {
            return iconName;
        }

        public void setIconName(String iconName) {
            this.iconName = iconName;
        }

        public int getKeepAliveInSeconds() {
            return keepAliveInSeconds;
        }

        public void setKeepAliveInSeconds(int keepAliveInSeconds) {
            this.keepAliveInSeconds = keepAliveInSeconds;
        }
    }

    public String toJson() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "";
        }
    }
}
