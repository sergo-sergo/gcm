<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href=<c:url value="/css/gcm.css" />>
    <title>GCM</title>
</head>
<body>

  <h1>GCM Push Notifications</h1>

  <%-- form --%>
  <form:form method="POST" commandName="pushNotification">

    <form:label path="to">To: (topic or token)</form:label>
    <form:input id="to" path="to" />
    <br/>
    <form:label path="title">Title:</form:label>
    <form:input id="title" path="title" />
    <br/>
    <form:label path="content">Content:</form:label>
    <form:input id="content" path="content" />
    <br/>

    <form:label path="iconName">Icon:</form:label>
    <form:input id="iconName" path="iconName" />
    <%--<img alt="time_bonus" src="<c:url value="/images/time_bonus.png"/>"/>--%>

    <br/>
    <form:label path="keepAliveInSeconds">Keep alive in (seconds):</form:label>
    <form:input id="keepAliveInSeconds" path="keepAliveInSeconds" value="-1" />
    <br/>
    <input id="push" type="submit" value="PUSH" />
  </form:form>

  <%-- table --%>
  <%--@elvariable id="notifications" type="java.util.List"--%>
  <%--@elvariable id="notification" type="com.gctech.gcm.model.GcmPushNotification"--%>
  <table>
    <tr>
      <th>Time</th>
      <th>destination</th>
      <th>title</th>
      <th>content</th>
      <th>iconName</th>
    </tr>
    <c:forEach items="${notifications}" var="notification">
      <tr>
        <td><c:out value="${notification.time}"/></td>
        <td><c:out value="${notification.to}"/></td>
        <td><c:out value="${notification.title}"/></td>
        <td><c:out value="${notification.content}"/></td>
        <td><c:out value="${notification.iconName}"/></td>
      </tr>
    </c:forEach>
  </table>

</body>
</html>
