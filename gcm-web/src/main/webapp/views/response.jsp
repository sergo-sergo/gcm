<%--@elvariable id="response" type="java.lang.String"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>

    <spring:url value="/" var="home" />
    <spring:url value="/css/gcm.css" var="stylesheet" />

    <head>
        <link rel="stylesheet" type="text/css" href="${stylesheet}">
        <title>GCM</title>
    </head>

    <body>
      <h1>GCM Push Notifications</h1>
      <h3>${response}</h3>
      <a href="${home}">Back</a>
    </body>
</html>
