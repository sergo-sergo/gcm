package com.gctech.gcm.web.controllers;

import com.gctech.gcm.dao.impl.GcmPushNotificationsDao;
import com.gctech.gcm.model.GcmPushNotification;
import com.gctech.gcm.service.GcmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping({"/"})
public class GcmController {

    @Autowired
    private GcmService gcmService;

    @Autowired
    private GcmPushNotificationsDao notificationsDao;

    public void setGmsService(GcmService gmsService) {
        this.gcmService = gmsService;
    }

    public void setNotificationsDao(GcmPushNotificationsDao notificationsDao) {
        this.notificationsDao = notificationsDao;
    }

    @RequestMapping(method = GET)
    public String get(Model model) {
        model.addAttribute("notifications", notificationsDao.getAll());
        model.addAttribute("pushNotification", new GcmPushNotification());
        return "gcm";
    }

    @RequestMapping(method = POST)
    public String post(GcmPushNotification gcmPushNotification, Model model) {
        model.addAttribute("response", gcmService.post(gcmPushNotification));
        notificationsDao.save(gcmPushNotification);
        return "response";
    }
}
