package com.gctech.gcm.web.configs;

import com.gctech.gcm.dao.configs.DaoConfig;
import com.gctech.gcm.service.configs.ServiceConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ServiceConfig.class, DaoConfig.class})
public class RootConfig {



}
