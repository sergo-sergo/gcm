package com.gctech.gcm.web.configs.security;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

public abstract class SecurityConfig extends WebSecurityConfigurerAdapter {

    protected abstract void configure(HttpSecurity http) throws Exception;
    protected abstract void configure(AuthenticationManagerBuilder auth) throws Exception;

}
