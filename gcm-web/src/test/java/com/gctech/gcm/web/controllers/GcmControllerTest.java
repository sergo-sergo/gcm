package com.gctech.gcm.web.controllers;

import com.gctech.gcm.dao.impl.GcmPushNotificationsDao;
import com.gctech.gcm.model.GcmPushNotification;
import com.gctech.gcm.service.GcmService;
import com.gctech.gcm.web.configs.RootConfig;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.view.InternalResourceView;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class GcmControllerTest {

    private GcmService gcmService;
    private GcmPushNotificationsDao notificationsDao;
    private GcmController controller;

    @Before
    public void setup() {
        gcmService = mock(GcmService.class);
        notificationsDao = mock(GcmPushNotificationsDao.class);
        controller = new GcmController();
        controller.setGmsService(gcmService);
        controller.setNotificationsDao(notificationsDao);
    }

    @Test
    public void getTest() throws Exception {
        MockMvc mockMvc = getMockMvc(controller);
        mockMvc.perform(get("/"))
                .andExpect(view().name("gcm"))
                .andExpect(model().attributeExists("notifications"))
                .andExpect(model().attributeExists("pushNotification"));

        verify(notificationsDao, times(1)).getAll();
    }

    @Test
    @Ignore
    public void postTest() throws Exception {
        // todo
        GcmPushNotification anyNotification = new GcmPushNotification();
        anyNotification.setTo("to");
        anyNotification.setTitle("title");
        anyNotification.setContent("content");
        anyNotification.setIconName("iconName");
        when(gcmService.post(anyNotification)).thenReturn("status:ok");

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.set("to", anyNotification.getTo());
        params.set("title", anyNotification.getTitle());
        params.set("content", anyNotification.getContent());
        params.set("iconName", anyNotification.getIconName());

        MockMvc mockMvc = getMockMvc(controller);
        mockMvc.perform(post("/").params(params))
                .andExpect(view().name("response"))
                .andExpect(model().attributeExists("response"));

        verify(notificationsDao, times(1)).getAll();
        verify(gcmService, times(1)).post(anyNotification);
    }

    private MockMvc getMockMvc(GcmController controller) {
        return standaloneSetup(controller)
                .setSingleView(new InternalResourceView("/views/*.jsp"))
                .build();
    }

}