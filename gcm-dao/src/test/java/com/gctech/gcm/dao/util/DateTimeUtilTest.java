package com.gctech.gcm.dao.util;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;

import static com.gctech.gcm.dao.util.DateTimeUtil.getFullDateTimeAsString;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class DateTimeUtilTest {

    @Test
    public void testGetFullDateTimeAsString() throws Exception {
        LocalDateTime dateTime = LocalDateTime.of(2016, Month.OCTOBER, 4, 12, 8);
        String result = getFullDateTimeAsString(dateTime);
        assertThat(result, containsString("04"));
        assertThat(result, containsString("OCTOBER"));
        assertThat(result, containsString("12:08:00"));
    }
}