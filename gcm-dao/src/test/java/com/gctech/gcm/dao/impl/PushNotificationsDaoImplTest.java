package com.gctech.gcm.dao.impl;

import com.gctech.gcm.dao.impl.GcmPushNotificationsDao;
import com.gctech.gcm.dao.impl.GcmPushNotificationsDaoImpl;
import com.gctech.gcm.model.GcmPushNotification;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class PushNotificationsDaoImplTest {

    private GcmPushNotificationsDao dao;

    @Before
    public void setup() {
        dao = new GcmPushNotificationsDaoImpl();
    }

    @Test
    public void testSave_whenFirstSave_thenOneItemInArray() {
        assertThat(dao.getAll().size(), is(0));
        dao.save(new GcmPushNotification());
        assertThat(dao.getAll().size(), is(1));
    }

    @Test
    public void testSave_whenLastIn_thenFirstOut() {
        GcmPushNotification firstNotification = new GcmPushNotification();
        GcmPushNotification secondNotification = new GcmPushNotification();
        dao.save(firstNotification);
        dao.save(secondNotification);
        assertThat(dao.getAll().get(dao.getAll().size() - 1), is(firstNotification));
    }

    @Test
    public void testSave_whenLimitReached_thenFirstIsRemoved() {
        GcmPushNotification firstNotification = new GcmPushNotification();
        dao.save(firstNotification);
        reachLimit();
        dao.save(new GcmPushNotification());
        assertThat(dao.getAll().get(dao.getAll().size() - 1), is(not(firstNotification)));
    }

    private void reachLimit() {
        while (dao.getAll().size() <= 10) {
            dao.save(new GcmPushNotification());
        }
    }

}