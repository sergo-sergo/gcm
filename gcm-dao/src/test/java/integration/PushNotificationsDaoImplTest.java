package integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gctech.gcm.dao.impl.GcmPushNotificationsDao;
import com.gctech.gcm.dao.impl.GcmPushNotificationsDaoImpl;
import com.gctech.gcm.model.GcmPushNotification;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PushNotificationsDaoImplTest {

    private static File templateFile;
    private GcmPushNotificationsDao dao;

    @BeforeClass
    public static void setup() throws IOException, URISyntaxException {
        templateFile = new File(PushNotificationsDaoImplTest.class.getResource("/test_templates.json").getFile());
    }

    @Before
    public void eachSetup() {
        dao = new GcmPushNotificationsDaoImpl();
    }

    @Test
    public void testMarshall_whenFileNotFound_thenEmptyList() {
        List<GcmPushNotification> result = dao.getFromResourceFile(new File("no_such_file"));
        assertThat(result.size(), is(0));
    }

    @Test
    public void testMarshall_whenArrayWithTwoItems_thenTwoJsonObjects() throws IOException {
        List<GcmPushNotification> notifications = dao.getFromResourceFile(templateFile);
        assertThat(notifications.size(), is(2));
    }

    @Test
    public void testMarshall_whenReadJsonFile_thenHasCorrectKeys() throws IOException {
        List<GcmPushNotification> notifications = dao.getFromResourceFile(templateFile);
        GcmPushNotification notification = notifications.get(0);
        String content = new ObjectMapper().writeValueAsString(notification);
        assertThat(content, containsString("to"));
        assertThat(content, containsString("data"));
        assertThat(content, containsString("title"));
        assertThat(content, containsString("iconName"));
        assertThat(content, containsString("keepAliveInSeconds"));
        assertThat(content, containsString("time_to_live"));
    }

}

