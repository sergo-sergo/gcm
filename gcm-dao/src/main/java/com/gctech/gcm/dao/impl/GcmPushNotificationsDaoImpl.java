package com.gctech.gcm.dao.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gctech.gcm.model.GcmPushNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.gctech.gcm.dao.util.DateTimeUtil.getFullDateTimeAsString;
import static java.time.LocalDateTime.now;
import static java.util.Collections.unmodifiableList;

@Repository
public class GcmPushNotificationsDaoImpl implements GcmPushNotificationsDao {

    private static final Logger logger = LoggerFactory.getLogger(GcmPushNotificationsDaoImpl.class);

    private final int LIMIT = 10;
    private final int DESTINATION_LIMIT_SYMBOLS = 60;

    private List<GcmPushNotification> notifications = new ArrayList<>();

    @Override
    public void save(GcmPushNotification notification) {
        if (notifications.size() > LIMIT) notifications.remove(notifications.size() - 1);
        notification.setTime(getFullDateTimeAsString(now()));
        if (notification.getTo().length() > DESTINATION_LIMIT_SYMBOLS) {
            notification.setTo(notification.getTo().substring(0, DESTINATION_LIMIT_SYMBOLS));
        }
        notifications.add(0, notification);
    }

    @Override
    public List<GcmPushNotification> getAll() {
        return unmodifiableList(notifications);
    }

    @Override
    public List<GcmPushNotification> getFromResourceFile(File file) {
        try {
            return unmodifiableList(new ObjectMapper().readValue(file, new TypeReference<List<GcmPushNotification>>() {}));
        } catch (IOException e) {
            logger.warn(String.format("get from resource file, error: %s", e.getMessage()));
            return unmodifiableList(new ArrayList<>());
        }
    }
}
