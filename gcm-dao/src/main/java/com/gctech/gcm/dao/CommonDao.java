package com.gctech.gcm.dao;

import java.io.Serializable;
import java.util.List;

public interface CommonDao<T extends Serializable> {

    List<T> getAll();
    void save(T t);

}
