package com.gctech.gcm.dao.impl;

import com.gctech.gcm.dao.CommonDao;
import com.gctech.gcm.model.GcmPushNotification;

import java.io.File;
import java.util.List;

public interface GcmPushNotificationsDao extends CommonDao<GcmPushNotification> {

    List<GcmPushNotification> getFromResourceFile(File file);

}
