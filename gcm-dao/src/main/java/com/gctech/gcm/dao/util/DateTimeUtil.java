package com.gctech.gcm.dao.util;

import java.time.LocalDateTime;

public class DateTimeUtil {

    public static String getFullDateTimeAsString(LocalDateTime dateTime) {
        return String.format("%s %s - %s:%s:%s",
                getTimeUnit(dateTime.getDayOfMonth()),
                dateTime.getMonth().toString(),
                getTimeUnit(dateTime.getHour()),
                getTimeUnit(dateTime.getMinute()),
                getTimeUnit(dateTime.getSecond()));
    }

    private static String getTimeUnit(int time) {
        String value = String.valueOf(time);
        return time < 10 ? "0" + value : value;
    }

}
