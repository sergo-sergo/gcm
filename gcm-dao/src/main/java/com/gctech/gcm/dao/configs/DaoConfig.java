package com.gctech.gcm.dao.configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.gctech.gcm.dao.impl"})
public class DaoConfig {



}
