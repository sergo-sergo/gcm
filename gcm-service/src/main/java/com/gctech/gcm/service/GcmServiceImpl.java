package com.gctech.gcm.service;

import com.gctech.gcm.model.GcmPushNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GcmServiceImpl implements GcmService {

    @Value("${url}") private String URL;

    @Autowired
    private RestTemplate rest;
    @Autowired
    private HttpHeaders headers;

    @Override
    public String post(GcmPushNotification notification) {
        return post(notification.toJson());
    }

    @Override
    public String post(String json) {
        ResponseEntity<String> responseEntity = rest.exchange(URL, HttpMethod.POST, new HttpEntity<>(json, headers), String.class);
        return String.format("response, status = %s, body = %s", responseEntity.getStatusCode(), responseEntity.getBody());
    }

}
