package com.gctech.gcm.service;

public interface RestMappedService<T> extends RestService {

    String post(T t);

}
