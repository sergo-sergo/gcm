package com.gctech.gcm.service.aspects;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAspect {

    private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Pointcut("execution(* com.gctech.gcm.service.GcmService.post(String)) && args(json)")
    public void post(String json) {}

    @Before("post(json)")
    public void beforePost(String json) {
        logger.info(String.format("[GcmServiceImpl::post] request, body = %s", json));
    }

    @AfterReturning(value = "post(json)", returning = "response")
    public void afterPost(String json, String response) {
        logger.info(String.format("[GcmServiceImpl::post] response = %s", response));
    }

}
