package integration;

import com.gctech.gcm.model.GcmPushNotification;
import com.gctech.gcm.service.GcmService;
import com.gctech.gcm.service.configs.ServiceConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceConfig.class})
public class LoggingAspectTest {

    @Autowired
    private GcmService gcmService;

    @Test
    public void testAspect() {
        GcmPushNotification notification = new GcmPushNotification();
        notification.setTo("cW8b3beXOFw:APA91bE99kGhGsg6zf4BkD8G6Frv5U2gaipT-wIv2z46ZNLhxntDeb_3WtIgJXFXSkmjz1oO046tTasNT7uMK_3rYE2v9oLAdVaHhq-vyU5hmJgyGRf8sMNiasv9NXRVS3ZvrqNEb7Fj");
        notification.setTitle("timeBonusTitle");
        notification.setContent("timeBonusContent");
        notification.setIconName("time_bonus");
        notification.setKeepAliveInSeconds(-1);
        gcmService.post(notification.toJson());
    }

}