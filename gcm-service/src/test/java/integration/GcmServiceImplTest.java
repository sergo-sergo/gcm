package integration;

import com.gctech.gcm.model.GcmPushNotification;
import com.gctech.gcm.service.GcmService;
import com.gctech.gcm.service.configs.ServiceConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceConfig.class})
public class GcmServiceImplTest {

    private GcmPushNotification emptyNotification;
    private GcmPushNotification fullNotification;

    @Autowired
    private GcmService service;

    @Before
    public void setup() {
        emptyNotification = new GcmPushNotification();

        fullNotification = new GcmPushNotification();
        fullNotification.setTime("00:00:00");
        fullNotification.setTo("/topics/integration_test_topic");
        fullNotification.setContent("content");
        fullNotification.setTitle("title");
        fullNotification.setKeepAliveInSeconds(180);
        fullNotification.setIconName("icon_name");
    }

    @Test
    public void testPost_whenIncorrectModelProvided_thenGetErrorMessage() {
        String result = service.post(emptyNotification);
        assertThat(result, containsString("MissingRegistration"));
    }

    @Test
    public void testPost_whenIncorrectTokenProvided_thenGetErrorMessage() {
        fullNotification.setTo("any symbols count can be here, but only client app can send the correct token to us");
        String result = service.post(fullNotification);
        assertThat(result, containsString("InvalidRegistration"));
    }

    @Test
    public void testPost_whenCorrectModelProvided_thenNotificationSuccessfullySend() {
        String result = service.post(fullNotification);
        final String CORRECT_RESPONSE_CONTAINS_THIS_STRING = "message_id";
        assertThat(result, containsString(CORRECT_RESPONSE_CONTAINS_THIS_STRING));
    }

}
